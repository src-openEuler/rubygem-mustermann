%global gem_name mustermann
%{?_with_bootstrap: %global bootstrap 1}
Name:                rubygem-%{gem_name}
Version:             1.1.1
Release:             2
Summary:             Your personal string matching expert
License:             MIT
URL:                 https://github.com/sinatra/mustermann
Source0:             https://rubygems.org/gems/%{gem_name}-%{version}.gem
# Support and mustermann-contrib routines required by test suite.
# git clone https://github.com/sinatra/mustermann.git && cd mustermann
# git checkout v1.1.1 && tar czvf mustermann-1.1.1-support.tgz support/
Source1:             %{gem_name}-%{version}-support.tgz
# tar czvf mustermann-1.1.1-mustermann-contrib.tgz mustermann-contrib/
Source2:             %{gem_name}-%{version}-mustermann-contrib.tgz
Patch0:              %{gem_name}-2.0.2-ruby32-regex_match-for-object.patch
# Similarly, from https://github.com/sinatra/mustermann/pull/113
Patch1:              %{gem_name}-1.1.1-ruby32-regex_match-for-object-2.patch
BuildRequires:       ruby(release) rubygems-devel ruby >= 2.2.0 rubygem(rspec) rubygem(rspec-its)
%if ! 0%{?bootstrap}
BuildRequires:       rubygem(sinatra)
%endif
BuildRequires:       rubygem(rack-test)
BuildArch:           noarch
%description
A library implementing patterns that behave like regular expressions.

%package             doc
Summary:             Documentation for %{name}
Requires:            %{name} = %{version}-%{release}
BuildArch:           noarch
%description         doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version} -b 1 -b 2
%patch0 -p2
%patch1 -p2

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%if ! 0%{?bootstrap}

%check
sed -i "/^require 'tool\/warning_filter'/ s/^/#/" \
  %{_builddir}/support/lib/support/env.rb
sed -i "/^require 'support\/coverage'/ s/^/#/" \
  %{_builddir}/support/lib/support.rb
pushd .%{gem_instdir}
mv spec/extension_spec.rb{,.disabled}
sed -i "/^require 'mustermann\/extension'/ s/^/#/" \
  spec/mustermann_spec.rb
sed -i '/^  describe :extend_object do$/,/^  end$/ s/^/#/' \
  spec/mustermann_spec.rb
rspec -I%{_builddir}/{support,mustermann-contrib}/lib spec
popd
%endif

%files
%dir %{gem_instdir}
%license %{gem_instdir}/LICENSE
%{gem_libdir}
%exclude %{gem_instdir}/mustermann.gemspec
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README.md
%{gem_instdir}/bench
%{gem_instdir}/spec

%changelog
* Mon Aug 14 2023 bizhiyuan <bizhiyuan@kylinos.cn> - 1.1.1-2
- fix regex_match error and build fail

* Thu Feb 24 2022 liyanan <liyanan32@huawei.com> - 1.1.1-1
- update to 1.1.1

* Tue Sep 8 2020 yanan li <liyanan032@huawei.com> - 1.0.2-2
- fix build fail

* Wed Aug 19 2020 geyanan <geyanan2@huawei.com> - 1.0.2-1
- package init

